package com.i5e2.likeawesomevegetable.repository;

import com.i5e2.likeawesomevegetable.domain.user.CompanyFile;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompanyFileJpaRepository extends JpaRepository<CompanyFile, Long> {
}
