package com.i5e2.likeawesomevegetable.repository;

import com.i5e2.likeawesomevegetable.domain.user.Subcribe;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SubcribeJpaRepository extends JpaRepository<Subcribe, Long> {
}
