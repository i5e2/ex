package com.i5e2.likeawesomevegetable.domain.alarm;

public enum AlarmDetail {
    LIKE, // 게시글에 좋아요
    AUCTION, // 경매 종료
    BUYING // 모집 종료
}
