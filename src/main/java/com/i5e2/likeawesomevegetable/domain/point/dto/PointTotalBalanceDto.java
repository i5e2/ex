package com.i5e2.likeawesomevegetable.domain.point.dto;

public interface PointTotalBalanceDto {
    Long getUserTotalBalance();

    Long getUserId();
}
