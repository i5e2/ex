package com.i5e2.likeawesomevegetable.domain.map;

public interface FarmAddress {

    Long getId();
    String getFarmName();
    String getFarmAddress();
}
