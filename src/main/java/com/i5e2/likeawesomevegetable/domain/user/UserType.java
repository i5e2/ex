package com.i5e2.likeawesomevegetable.domain.user;

import lombok.Getter;

@Getter
public enum UserType {
    ROLE_BASIC,
    ROLE_COMPANY,
    ROLE_FARM,
    ROLE_ADMIN;

}
