package com.i5e2.likeawesomevegetable.domain.user.inquiry;

public interface CompanyImageLink {

    String getCompanyImageLink();
}
