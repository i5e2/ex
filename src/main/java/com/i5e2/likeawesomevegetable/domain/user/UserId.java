package com.i5e2.likeawesomevegetable.domain.user;

public interface UserId {
    Long getUserId();
}
