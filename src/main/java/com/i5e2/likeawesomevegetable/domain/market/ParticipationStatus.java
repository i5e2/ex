package com.i5e2.likeawesomevegetable.domain.market;

import lombok.Getter;

@Getter
public enum ParticipationStatus {

    UNDERWAY,
    BEFORE,
    END
}