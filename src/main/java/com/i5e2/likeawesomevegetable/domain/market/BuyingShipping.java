package com.i5e2.likeawesomevegetable.domain.market;

import lombok.Getter;

@Getter
public enum BuyingShipping {
    BOXING, TONBAG, CONTIBOX
}
