package com.i5e2.likeawesomevegetable.domain.contract;

public enum ContractType {
    BUYING,
    AUCTION
}
