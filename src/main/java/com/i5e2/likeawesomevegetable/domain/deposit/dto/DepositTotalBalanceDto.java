package com.i5e2.likeawesomevegetable.domain.deposit.dto;

public interface DepositTotalBalanceDto {
    Long getDepositTotalAmount();
}
