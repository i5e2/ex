package com.i5e2.likeawesomevegetable.domain.verification;

public enum Verification {
    VERIFIED,
    NOT_VERIFIED
}
